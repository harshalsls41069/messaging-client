export class CommunicationLog {
    public id: number;
    public conflictId: number;
    public message?: string;
    public commLogCreatedBy;
    public commLogCreatedByUserName;
    public userFirstName;
    public userLastName;
    public userRoleName;
    public commLogCreatedOn;
    public attachmentList: CommunicationLogAttachment[] = [];

}


export class CommunicationLogAttachment {
    public id: number;
    public communicationLogId: number;
    public isActive: string;
    public attachedFileName: string;
    public conflictId?: number;
}
