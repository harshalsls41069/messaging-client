import { Component, OnInit } from '@angular/core';
import { User} from '../common/model/user';
import { NgForm } from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public screenName: string;
  public isLostPassword: boolean = false;
  public UserName: string;
  public Password: string;
  returnUrl: string;
  returnMsg: string;
  public user: User;
  public onLoginPage: boolean = true;
  public loading = false;
  public userImageUrl;
  public errFun;
  constructor(private loginService: LoginService) {

    this.user = new User();

  }

  ngOnInit() {
  }



  /**
   * SR@20180130 : Call this method on click of Login Button
   */
  onLogin() {

    this.loginService.logIn(this.user).subscribe(
      res => {

        },
      error => {

      }
    );
  }

}
