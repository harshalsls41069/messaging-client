import { Component, OnInit } from '@angular/core';
import { CommunicationLog } from '../common/model/communicationLog.model';
import { User } from '../common/model/user';
import { NgForm } from '../../../node_modules/@angular/forms';
import { MessagingService } from './messaging.service';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.scss']
})
export class MessagingComponent implements OnInit {

  public logArray;
  logFound = true;
  public formData;
  public isSavingRecord;
  public communicationLog: CommunicationLog;


  constructor(private messagingService: MessagingService) {
    this.communicationLog = new CommunicationLog();
  }

  ngOnInit() {
    this.getCommunicationLog();
  }

  public getCommunicationLog() {
    this.logFound = true;
    // this.messagingService.getCommunicationLog(this.conflict)
    //   .subscribe(
    //   response => {
    //     console.log(response);
    //     this.logArray = response;
    //     this.scrollToBottom();
    //   },
    //   error => {
    //     console.log(error);
    //     this.logFound = false;
    //   });
  }


  public onSubmitLog(ngForm: NgForm) {
    if (ngForm.valid) {
      this.callSubmitLog(ngForm);
    }
  }
  public onResetCommentLog(ngForm: NgForm) {
    this.formData = new FormData();
    ngForm.resetForm();
  }

  public onRefreshLog() {
    this.getCommunicationLog();
  }


  public callSubmitLog(form: NgForm) {
    this.communicationLog.message = this.communicationLog.message == undefined ? '' : this.communicationLog.message;
 
    this.formData.append('message', this.communicationLog.message);
   // this.formData.append('conflictId', this.user.conflictId);
    this.isSavingRecord = true;
    this.messagingService.saveCommunicationLog(this.formData)
      .subscribe(
      response => {
        this.isSavingRecord = false;
        this.onResetCommentLog(form);
        this.getCommunicationLog();

      },
      error => {
        console.log(error);
        this.isSavingRecord = false;
      });
  }

  scrollToBottom() {
    var obj = document.getElementById('commLog');
    if (obj != undefined && obj.scrollHeight != undefined) {
      obj.scrollTop = obj.scrollHeight;
    }
  }








}
