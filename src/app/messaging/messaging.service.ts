import { Injectable } from '@angular/core';
import { HttpClientService } from '../common/services/http-client.service';
import { Observable } from '../../../node_modules/rxjs';
import { WebServiceConstant } from '../common/services/web-constant.service';
import { User } from '../common/model/user';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  constructor(private httpClientService: HttpClientService) { }



  getCommunicationLog(user: User): Observable<User> {
    return this.httpClientService.post(WebServiceConstant.GET_COMMUNICATION_LOG, user)
      .map(this.httpClientService.extractData)
      .catch(this.httpClientService.handleError);
  }


  saveCommunicationLog(user: User): Observable<User> {
    return this.httpClientService.post(WebServiceConstant.GET_COMMUNICATION_LOG, user)
      .map(this.httpClientService.extractData)
      .catch(this.httpClientService.handleError);
  }

}
